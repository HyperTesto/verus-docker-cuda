FROM nvidia/cuda:10.1-devel-centos7

LABEL maintainer="info@hypertesto.me"

RUN yum -y -q install git gcc gcc-c++ make wget autoconf automake install openssl-devel libcurl-devel zlib-devel

# Create a user to do the build
ENV BUILD_FOLDER=/minerbuild
ENV APP_FOLDER=/app
ENV APP_USER=minerbuilder
ENV CCMINER_VERSION=verus2.2gpu

RUN adduser $APP_USER && \
    mkdir $BUILD_FOLDER && \
    chown $APP_USER.users $BUILD_FOLDER

# We'll build jansson as regular user
USER $APP_USER

# Download and build jansson
RUN cd $BUILD_FOLDER && \
    wget http://www.digip.org/jansson/releases/jansson-2.10.tar.gz

RUN cd $BUILD_FOLDER && \
    tar -xvf jansson-2.10.tar.gz && \
    rm jansson-2.10.tar.gz

RUN cd $BUILD_FOLDER/jansson-2.10 && \
    ./configure && \
    make && \
    make check

# Install Jansson (need root for this)
USER root
RUN cd $BUILD_FOLDER/jansson-2.10 && \
    make install

# Now switch to the builder and check out the git repo
USER $APP_USER

# Clone ccminer from the git repo and build it
RUN cd $BUILD_FOLDER && \
    git clone https://github.com/monkins1010/ccminer.git --branch $CCMINER_VERSION --single-branch

ENV CCMINER_FOLDER=$BUILD_FOLDER/ccminer
ENV CCMINER_FOLDER=$BUILD_FOLDER/ccminer

RUN cd $CCMINER_FOLDER && \
    sed -i \
     -e 's/#nvcc_ARCH += -gencode=arch=compute_61/nvcc_ARCH += -gencode=arch=compute_61/' \
     -e 's/#nvcc_ARCH += -gencode=arch=compute_35/nvcc_ARCH += -gencode=arch=compute_35/' \
     -e 's/#nvcc_ARCH += -gencode=arch=compute_30/nvcc_ARCH += -gencode=arch=compute_30/' \
     Makefile.am

RUN export CXXFLAGS="-std=gnu++11"
RUN export CFLAGS="-std=gnu11"

RUN cd $CCMINER_FOLDER && \
    ./autogen.sh && ./configure.sh && \
    sed -i -e 's/gnu99/gnu11/g' -e 's/g++/g++ -std=gnu++11/g' Makefile && \
    make -j$(nproc)

USER root
RUN mkdir $APP_FOLDER && \
    chown $APP_USER.users $APP_FOLDER && \
    cp $CCMINER_FOLDER/ccminer $APP_FOLDER

# RUNTIME CONTAINER
FROM nvidia/cuda:10.1-runtime-centos7

# Redefine the app user and folder - note app folder must be the same as the first stage
ENV APP_FOLDER=/app
ENV APP_USER=miner

# Copy the stuff that we built
COPY --from=0 $APP_FOLDER $APP_FOLDER
COPY --from=0 /usr/local/lib /usr/local/lib

# Get the non-devel versions of the libraries that we need
RUN yum -y -q install openssl libcurl zlib libgomp &&  \
    yum clean all && \
    rm -rf /var/cache/yum

# Load the Jansson library that's now built
RUN echo /usr/local/lib > /etc/ld.so.conf.d/userlocal.conf && \
    ldconfig

# Symlink the app to /usr/local/bin
RUN ln -s $APP_FOLDER/ccminer /usr/local/bin/ccminer && \
    chown -R root.root $APP_FOLDER

# Recreate and switch to the app user for this build
RUN adduser $APP_USER
USER $APP_USER

ENTRYPOINT [ "ccminer" ]
CMD ["-h"]
