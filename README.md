# Verus Docker CUDA
![license](https://img.shields.io/badge/license-Apache-informational) ![pulls](https://badgen.net/docker/pulls/hypertesto/verus-docker-cuda) ![size](https://badgen.net/docker/size/hypertesto/verus-docker-cuda) ![CUDA](https://img.shields.io/badge/CUDA-10.1-informational) ![discord](https://img.shields.io/badge/Discord-Bluhsfrunz%230085-informational)

_Docker + CUDA + Verus ccminer = ❤_

Docker image with VerusCoin ccminer for GPU based on [monkins1010 ccminer fork](https://github.com/monkins1010/ccminer).

This work is heavily inspired by [patrickceg/ccminer](https://hub.docker.com/r/patrickceg/ccminer/).

I thank monkins1010, patrickceg and the Verus community for all their respective efforts.


## Prerequisites

* A CUDA-enabled GPU with recent [drivers](https://github.com/NVIDIA/nvidia-docker/wiki/Frequently-Asked-Questions#how-do-i-install-the-nvidia-driver) (The image is built and tested against CUDA 10.2)
* [NVIDIA Container Toolkit](https://github.com/NVIDIA/nvidia-docker) installed on the system
* A recent version of [Docker](https://www.docker.com/) (at least 19.03)

## Usage
Assuming you are running as root or as a user that is part of the docker group):

**Print ccminer help**:

```bash
$ docker run --rm --gpus all hypertesto/verus-docker-cuda
```

**Start verus-miner as daemon**:

```bash
$ docker run -d --name verus-miner --gpus all hypertesto/verus-docker-cuda -a verus -o stratum+tcp://pool_address:pool_port -u your_wallet_addres.your_worker_name -p x -i 12
```

**Print latest container logs**:  
Assuming you named the container `verus-miner` as in the example above.

```bash
$ docker logs verus-miner
```

**Watch live container logs**:  
Assuming you named the container `verus-miner` as in the example above.

```bash
$ docker logs -f verus-miner
```

## Building the Docker image

```bash
$ git clone https://gitlab.com/HyperTesto/verus-docker-cuda.git
$ cd verus-docker-cuda
$ docker build -t verus-docker-cuda .
```

## Test
I've tested this sucessfully on this environment:

```
OS: Ubuntu server 18.04
GC: RTX 2060 Super
NVIDIA Driver: 440.100
CUDA: 10.2
Docker: 19.03.12
```

## Support
Reach out to me at one of the following places:

* on [issues page](https://gitlab.com/HyperTesto/verus-docker-cuda/-/issues) in the project repo
* `Bluhsfrunz#0085` on Discord

## Donations
If you find this tool useful and like to support its  development, then consider a donation.

* VRSC: `RPv2eD5d1327DYfLb4fjZASZtibjuXf6mT`
* BMC: https://www.buymeacoffee.com/hypertesto
